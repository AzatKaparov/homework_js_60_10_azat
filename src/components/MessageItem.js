import React from 'react';

const MessageItem = ({ message, author, date }) => {
    return (
        <div className="message-item">
            <h4 className="message-author">{author}
            <span className="message-date">{date}</span>
            </h4>
            <p className="message-text">{message}</p>
        </div>
    );
};

export default MessageItem;