import React, {useState, useEffect} from 'react';
import "./Messages.css";
import MessageItem from "../components/MessageItem";
import SendMessageForm from "../components/SendMessageForm";


const Messages = () => {
    const [lastDate, setLastDate] = useState(null);
    const [messages, setMessages] = useState([]);
    const url = 'http://146.185.154.90:8000/messages';
    const [sendMessage, setSendMessage] = useState('');

    const handleSendMessage = e => {
        setSendMessage(e.target.value);
    };

    const doSendMessage = async e => {
        e.preventDefault();
        const data = new URLSearchParams();
        data.set("message", sendMessage);
        data.set("author", "Azat");

        const response = await fetch('http://146.185.154.90:8000/messages', {
            method: 'post',
            body: data,
        });

        setSendMessage('');
        if (response.ok) {
            console.log("Message sent successfully");
        }
    };

    useEffect(() => {
        const fetchData = async () => {
            let responseUrl;
            if (lastDate !== null) {
                responseUrl = `${url}?datetime=${lastDate}`;
            } else {
                responseUrl = url;
            }

            const response = await fetch(responseUrl);

            if (response.ok) {
                const newMessages = await response.json();
                if (newMessages.length > 0) {
                    setMessages([...messages, ...newMessages]);
                    setLastDate(newMessages[newMessages.length - 1].datetime);
                }
            }
        };

        const interval = setInterval(fetchData, 3000);
        return () => clearInterval(interval);
    }, [lastDate, messages]);

    return (
        <div className="messages">
            <h1>Welcome to our chat!</h1>
            <SendMessageForm
                inpValue={sendMessage}
                onInpChange={handleSendMessage}
                submit={doSendMessage}
            />
            <div className="messages-block">
                {messages.map(item => {
                    return (
                        <MessageItem
                            key={item._id}
                            message={item.message}
                            author={item.author}
                            date={item.datetime}
                        />
                    )
                })}
            </div>
        </div>
    );
};

export default Messages;