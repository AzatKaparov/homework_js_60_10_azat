import './App.css'
import Messages from "./containers/Messages";

function App() {
  return (
    <div className="container">
      <Messages/>
    </div>
  );
}

export default App;
